
//var url_base = "http://192.168.0.20/CentrosComercialesWeb/";
var url_base="http://batman.apliko.co/";

//Variable para determinar si la apliacion tiene accesso a internet o no
var internet = true;

var offLine = function()
{
    internet = false;
    mostrarMensaje("<p>Esta aplicación necesita datos para poder funcionar</p>");
};
function onDeviceReady()
{
    document.addEventListener("offline", offLine, false);

}
;


document.addEventListener("deviceready", onDeviceReady, false);
function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
        x.src = x.oSrc;
}
function MM_preloadImages() { //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p)
            d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x;
    if (!d)
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all)
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++)
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++)
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById)
        x = d.getElementById(n);
    return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc)
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}
$(document).ready(
        function()
        {
            $(function()
            {
                onDeviceReady();
                var $menu = $('nav#menu');
                $menu.mmenu({
                    classes: "mm-fullscreen"
                });
                $menu.mmenu();
                $("#menu ul").css("display", "block");

            });
            $("#atras").click(
                    function(e)
                    {
                        console.log("Di clic atras");
                        e.preventDefault();
                        anterior();
                    }
            );
            $(".btconfig").click(
                    function(e)
                    {
                        e.preventDefault();
                        redirigir("busqueda/7.html");
                    }
            );
            $(".btrefresh").click(
                    function(e)
                    {
                        e.preventDefault();
                        //location.reload();

                    }
            );
            $("#logo").click(
                    function(e)
                    {
                        e.preventDefault();
                        redirigir("../index.html");
                    }
            );
            $("#btnAceptar").click(
                    function(e)
                    {
                        e.preventDefault();
                        if (!internet)
                        {
                            recargar();
                        } else {
                            $("#element_to_pop_up").css("background-color", "rgba(255,255,255,0);");
                            $("#contenidoPopUp").html('<img src="../images/loading.gif"  width="90px" height="55px" />  ');
                            $("#btnAceptar").css("display", "none");
                            $("#btnCancelar").css("display", "none");
                            $('#element_to_pop_up').bPopup().close();
                        }



                    });
            $("#btnCancelar").click(
                    function(e)
                    {
                        e.preventDefault();
                        $("#element_to_pop_up").css("background-color", "rgba(255,255,255,0);");
                        $("#contenidoPopUp").html('<img src="../images/loading.gif"  width="90px" height="55px" />  ');
                        $("#btnAceptar").css("display", "none");
                        $("#btnCancelar").css("display", "none");
                        $('#element_to_pop_up').bPopup().close();
                    });



        }
);

function checkInternet()
{
//    var networkState = navigator.connection.type;
//
//    var states = {};
//    states[Connection.UNKNOWN] = 'Unknown connection';
//    states[Connection.ETHERNET] = 'Ethernet connection';
//    states[Connection.WIFI] = 'WiFi connection';
//    states[Connection.CELL_2G] = 'Cell 2G connection';
//    states[Connection.CELL_3G] = 'Cell 3G connection';
//    states[Connection.CELL_4G] = 'Cell 4G connection';
//    states[Connection.CELL] = 'Cell generic connection';
//    states[Connection.NONE] = 'No network connection';

//    alert('Connection type: ' + states[networkState]);
    return true;
}
function salir()
{
    navigator.app.exitApp();
}
function anterior()
{
    window.history.back();
}
function recargar()
{
    location.reload();
}
function mostrarMensaje(men)
{
    $("#element_to_pop_up").css("background", "rgba(255,255,255,1)");
    $("#element_to_pop_up").css("width", "auto");
    $("#element_to_pop_up").css("height", "auto");
    $("#contenidoPopUp").html(men);
    $("#btnAceptar").css("display", "inline");
    mostrarDialogo();
}
function mostrarDialogo()
{
    $('#element_to_pop_up').bPopup();
}
function ocultarDialogo()
{
    console.log("ocultarDialogo");
    $('#element_to_pop_up').bPopup().close();
}
/*Esta funcion se encarga de realizar una llamada ajax y retornar el resultado, retorna null en caso de algun error
 var datos = {
 "id"     : blog.id,
 "name"   : blog.name,
 "url"    : blog.url,
 "author" : blog.author
 };
 
 */
function ajax(url2, datos, callback)
{
    checkInternet();
    var retornar = null;
    $.ajax({
        url: url2,
        type: "POST",
        timeout: 10000,
        data: datos,
        headers: {'Access-Control-Allow-Origin': '*'},
        crossDomain: true,
        error: function(jqXHR, textStatus, errorThrown)
        {
//            log("base", "ajax", "textStatus: " + textStatus);
//            log("base", "ajax", "errorThrown: " + imprimirObjeto(errorThrown));
//            console.log("err: " + jqXHR.responseText);
//            //callback(retornar);
//            setError(jqXHR.responseText);
//            //redirigir("../error.html");
            internet = false;
            mostrarMensaje("<p>No es posible comunicarse con el servidor en este momento, intenta mas tarde</p>");
        },
        success: function(data)
        {
            retornar = data;
        }
    }).done(function()
    {
        callback(retornar);
    });
}

function log(pagina, funcion, mensaje)
{
    console.log(pagina + "-" + funcion + "-" + mensaje);
}
/*Funcion que se encarga de obtener las variables que se envian por la url*/
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
/*Redirije la aplicacion a la url indicada*/
function redirigir(url)
{
    $(location).attr("href", url);
}
/*
 Esta funcion se encarga de obtener el banner correspondiente a la vista
 Parametros:
 Vista: Lugar desde donde se llama a la funcion
 cc: Id del centro comercial
 categoria: Categoria donde se encuentra
 tienda: Id de la tienda
 */

function getBanner(vista, idCentroComercial, idCategoria, idAlmacen, ruta2)
{
    console.log("Entre a obtener los banners vista: " + vista);
    var url = url_base + "banners/getbanners.xml";
    var ruta="";
    var datos = {
        vista: vista,
        idCentroComercial: idCentroComercial,
        idCategoria: idCategoria,
        idAlmacen: idAlmacen
    };
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    $("#slider").html("");
                    $("datos", xml).each(
                            function()
                            {
                                var obj = $(this).find("Banner");
                                var imagen = $("imagen", obj).text();
                                if (imagen)
                                {
                                    console.log("Ente al if");
                                    if (ruta2 == "null")
                                        ruta = "busqueda/";
                                    else
                                        ruta = "";
                                    var html = '<a href="' + ruta + '6b.html?vista=$2&idCentroComercial=$3&idCategoria=$4&idAlmacen=$5"><img src="$1" /></a>';
                                    html = html.replace("$1", imagen);
                                    html = html.replace("$2", vista);
                                    html = html.replace("$3", idCentroComercial);
                                    html = html.replace("$4", idCategoria);
                                    html = html.replace("$5", idAlmacen);
                                    $("#slider").append(html);
                                }

                            }
                    );
                    $('#slider').nivoSlider();
                } else {
                    console.log("Error al obtener banners");
                    console.log("Vista: " + vista);
                    console.log("idCentroComercial: " + idCentroComercial);
                    console.log("idCaregoria: " + idCategoria);
                    console.log("idAlmacen: " + idAlmacen);
                    console.log("ruta: " + ruta);

                }
            });



}
function imprimirObjeto(object)
{
    var output = '';
    for (var property in object) {
        output += property + ': ' + object[property] + '; ';
    }
    return output;


}
function xmlToString(xmlData) {

    var xmlString;
    //IE
    if (window.ActiveXObject) {
        xmlString = xmlData.xml;
    }
    // code for Mozilla, Firefox, Opera, etc.
    else {
        xmlString = (new XMLSerializer()).serializeToString(xmlData);
    }
    return xmlString;
}
function cambiarAcentos(texto)
{
    var regex = new RegExp('%20', 'g');
    texto = texto.replace(regex, ' ');
    //Url acentos minuscula
    regex = new RegExp('%C3%A1', 'g');
    texto = texto.replace(regex, '&aacute;');
    regex = new RegExp('%C3%A9', 'g');
    texto = texto.replace(regex, '&eacute;');
    regex = new RegExp('%C3%AD', 'g');
    texto = texto.replace(regex, '&iacute;');
    regex = new RegExp('%C3%B3', 'g');
    texto = texto.replace(regex, '&oacute;');
    regex = new RegExp('%C3%BA', 'g');
    texto = texto.replace(regex, '&uacute;');
    regex = new RegExp('%C3%B1', 'g');
    texto = texto.replace(regex, '&ntilde;');
    regex = new RegExp('%C3%BC', 'g');
    texto = texto.replace(regex, '&uuml;');

    //Url acentos mayuscula
    regex = new RegExp('%C3%81', 'g');
    texto = texto.replace(regex, '&Aacute;');
    regex = new RegExp('%C3%89', 'g');
    texto = texto.replace(regex, '&Eacute;');
    regex = new RegExp('%C3%8D', 'g');
    texto = texto.replace(regex, '&Iacute;');
    regex = new RegExp('%C3%93', 'g');
    texto = texto.replace(regex, '&Oacute;');
    regex = new RegExp('%C3%9A', 'g');
    texto = texto.replace(regex, '&Uacute;');
    regex = new RegExp('%C3%91', 'g');
    texto = texto.replace(regex, '&Ntilde;');
    regex = new RegExp('%C3%9C', 'g');
    texto = texto.replace(regex, '&Uuml;');

    //Acentos en minuscula
    regex = new RegExp('á', 'g');
    texto = texto.replace(regex, '&aacute;');
    regex = new RegExp('é', 'g');
    texto = texto.replace(regex, '&eacute;');
    regex = new RegExp('í', 'g');
    texto = texto.replace(regex, '&iacute;');
    regex = new RegExp('ó', 'g');
    texto = texto.replace(regex, '&oacute;');
    regex = new RegExp('&uacute;', 'g');
    texto = texto.replace(regex, '&uacute;');
    regex = new RegExp('ñ', 'g');
    texto = texto.replace(regex, '&ntilde;');
    regex = new RegExp('ü', 'g');
    texto = texto.replace(regex, '&Uuml;');
    //Acentos en mayuscula
    regex = new RegExp('Á', 'g');
    texto = texto.replace(regex, '&Aacute;');
    regex = new RegExp('É', 'g');
    texto = texto.replace(regex, '&Eacute;');
    regex = new RegExp('Í', 'g');
    texto = texto.replace(regex, '&Iacute;');
    regex = new RegExp('Ó', 'g');
    texto = texto.replace(regex, '&Oacute;');
    regex = new RegExp('&uacute;', 'g');
    texto = texto.replace(regex, '&Uacute;');
    regex = new RegExp('Ñ', 'g');
    texto = texto.replace(regex, '&Ntilde;');
    regex = new RegExp('Ü', 'g');
    texto = texto.replace(regex, '&Uuml;');

    return texto;

}
function cambiarAcentos2(texto)
{
    //Acentos en minuscula
    regex = new RegExp('&aacute;', 'g');
    texto = texto.replace(regex, 'á');
    regex = new RegExp('&eacute;', 'g');
    texto = texto.replace(regex, 'é');
    regex = new RegExp('&iacute;', 'g');
    texto = texto.replace(regex, 'í');
    regex = new RegExp('&oacute;', 'g');
    texto = texto.replace(regex, '&oacute;');
    regex = new RegExp('&uacute;', 'g');
    texto = texto.replace(regex, '&uacute;');
    regex = new RegExp('&ntilde;', 'g');
    texto = texto.replace(regex, 'ñ');
    regex = new RegExp('&uuml;', 'g');
    texto = texto.replace(regex, 'ü');
    //Acentos en mayuscula
    regex = new RegExp('&Aacute;', 'g');
    texto = texto.replace(regex, 'Á');
    regex = new RegExp('&Eacute;', 'g');
    texto = texto.replace(regex, 'É');
    regex = new RegExp('&Iacute;', 'g');
    texto = texto.replace(regex, 'Í');
    regex = new RegExp('&Oacute;', 'g');
    texto = texto.replace(regex, '&oacute;');
    regex = new RegExp('&Uacute;', 'g');
    texto = texto.replace(regex, '&uacute;');
    regex = new RegExp('&Ntilde;', 'g');
    texto = texto.replace(regex, 'Ñ');
    regex = new RegExp('&Uuml;', 'g');
    texto = texto.replace(regex, 'Ü');
    return texto;

}


/* ##################################### VARIABLES DE SESION ##################################### */
function crearVariableSesion(nombre, valor)
{
    localStorage.setItem(nombre, valor);
}
function obtenerVariable(nombre)
{
    var valor = localStorage.getItem(nombre);
    if (valor)
        return valor;
    else
        return null;
}
function removeVariable(nombre)
{
    localStorage.removeItem(nombre);
}

function getIdPais()
{
    return obtenerVariable("idPais");
}
function setIdPais(idPais)
{
    crearVariableSesion("idPais", idPais);
    //elimino la ciudad
    removeIdCiudad();
}

function getNombrePais()
{
    return obtenerVariable("nombrePais");
}
function setNombrePais(nombrePais)
{
    crearVariableSesion("nombrePais", nombrePais);
}
function getIdCiudad()
{
    return obtenerVariable("idCiudad");
}
function setIdCiudad(idCiudad)
{
    crearVariableSesion("idCiudad", idCiudad);
}
function removeIdCiudad()
{
    removeVariable("idCiudad");
}
function getNombreCiudad()
{
    return obtenerVariable("nombreCiudad");
}
function setNombreCiudad(nombreCiudad)
{
    crearVariableSesion("nombreCiudad", nombreCiudad);
}
function getError()
{
    return obtenerVariable("error");
}
function setError(error)
{
    crearVariableSesion("error", error);
}

/* ##################################### FIN VARIABLES DE SESION ##################################### */
function openUrl(url)
{
    navigator.app.loadUrl(url, {openExternal: true});
    //window.open(url, "_blank");
}