//Se encarga de listar las ciudades y mostrarlas en una lista
///* This code is used to run as soon as Intel activates */
//function onDeviceReady()
//{
//    //hide splash screen
//    
//    var parametros=getUrlVars();
//    getCiudades(parametros["url"]);
//};
//document.addEventListener("deviceready", onDeviceReady, false);


$(document).ready(function()
{
    /*Funciones autoejecutables*/
    (function()
    {
        console.log("entre a la autofuncion");
        var parametros = getUrlVars();
        console.log("nombrePais: " + parametros["nombrePais"]);
        if(parametros['url']=="1a.html"){
                $("#titulo").text("ENCUENTRA LA TIENDA QUE BUSCAS");
                $("#BT").attr("src", "../images/btBTImg.svg");
            }else{
                $("#titulo").text("ENCUENTRA EL CENTRO COMERCIAL QUE BUSCAS");
                $("#BT").attr("src", "../images/btBCCImg.svg");
            }
        getCiudades(parametros["url"], parametros["idPais"], parametros["nombrePais"],parametros["div"]);
    })();
    $(".menu").on("click", "li",
            function(e)
            {
                e.preventDefault();
                var url = $(this).find("a").attr("href");
                redirigir(url);
            });

});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}

/*Se encarga de obtener las ciudades de la base de datos y mostrarlas en un select*/
function getCiudades(urlD, idPais, nombrePais,div)
{
    mostrarDialogo();
    estaVacio = true;
    console.log("urlD: " + urlD);

    var url = url_base + "ciudades/index.xml";
    var datos = {
        idPais: idPais
    };

    console.log("url: " + url);
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
//            $("#ciudades").append("<option value='0'>Seleccione...</option>");
                    $("#ciudades").html("");
                    $("datos", xml).each(function()
                    {

                        var obj = $(this).find("Ciudade");
                        var valor, texto;
                        valor = $("id", obj).text();
                        texto = $("nombre", obj).text();
                        if (valor)
                        {
                            console.log("Valor: " + valor);
                            estaVacio = false;
                            var html = "<li class='todos'>" +
                                    "<a href='$1?idPais=$5&nombrePais=$6&idCiudad=$2&nombreCiudad=$3&div=$7' id='lnktodos'>$4</a>" +
                                    "<a class='go'></a>" +
                                    "</li>";
                            html = html.replace("$1", urlD);
                            html = html.replace("$2", valor);
                            html = html.replace("$3", texto);
                            html = html.replace("$4", texto);
                            html = html.replace("$5", idPais);
                            html = html.replace("$6", nombrePais);
                            html = html.replace("$7", div);
                            $("#ciudades").append(html);
                        }
                    });

                }
                ocultarDialogo();
                if (estaVacio)
                    $("#ciudades").html("Lo sentimos, no encontramos informacion");

            });


}