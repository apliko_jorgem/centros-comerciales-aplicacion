/*Esta pagina se encarga de listar los pisos del centro comercial */
function onDeviceReady() {

}
;
document.addEventListener("deviceready", onDeviceReady, false);
$(document).ready(
        function()
        {
            var parametros = getUrlVars();
            getBanner("5d", parametros["idCentroComercial"], "null", "null", "../");
            getPisos(parametros["idCentroComercial"]);
            $(".menu").on("click","li",
            function(e)
            {
                e.preventDefault();
                var url=$(this).find("a").attr("href");
                redirigir(url);
            });
        }
                
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getPisos(idCentroComercial)
{
    mostrarDialogo();
    var estaVacio = true;
    var url = url_base + "pisos/getpisosbycentrocomercial.xml";
    var datos = {
        idCentroComercial: idCentroComercial
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("datos", xml).each(function()
            {
                var obj = $(this).find("Piso");
                var nombre, id;
                nombre = $("nombre", obj).text();
                id = $("id", obj).text();
                if (id)
                {
                    estaVacio = false;
                    var html = "<li class='nivel' id='pisos'>" +
                            "<a href='5e.html?idPiso=$1' id='lnknivel'>$2</a>" +
                            "<a class='go'></a>" +
                            "</li>";
                    html = html.replace("$1", id);
                    html = html.replace("$2", nombre);
                    $("#pisos").append(html);
                }

            });
        }
        ocultarDialogo();
        if (estaVacio)
        {
            $("#pisos").append("Lo sentimos, no conseguimos datos");
        }
    });


}