/*Esta pagina se encarga de mostrar informacion de un centrocomercial en especifico*/
var idCentroComercial;
function onDeviceReady()
{
    //hide splash screen



}
;
document.addEventListener("deviceready", onDeviceReady, false);
$(document).ready(function()
{
    $(function()
    {
        var parametros = getUrlVars();
        idCentroComercial = parametros["idCentroComercial"];
        getBanner("5a", idCentroComercial, "null", "null", "../");
        getInfoCentroComercial(idCentroComercial);
    }
    );
    $(".menu").on("click", "li",
            function(e)
            {

                e.preventDefault();
                var id = $(this).find("a").attr("id");
                if (id != "masInformacion")
                {
                    var url = $(this).find("a").attr("href");
                    redirigir(url);
                } else {
                    var url = $(this).find("a").attr("href");
                    openUrl(url);
                }
            });
//    $("#tiendas").click(
//            function(e)
//            {
//                e.preventDefault();
//                var url = "5g.html?idCentroComercial=$1";
//                url = url.replace("$1", idCentroComercial);
//                redirigir(url);
//            }
//    );
//    $("#comoLlegar").click(
//            function(e)
//            {
//                e.preventDefault();
//                var url = "5b.html?idCentroComercial=$1";
//                url = url.replace("$1", idCentroComercial);
//                redirigir(url);
//            }
//    );
//    $("#mapas").click(
//            function(e)
//            {
//                e.preventDefault();
//                var url = "5d.html?idCentroComercial=$1";
//                url = url.replace("$1", idCentroComercial);
//                redirigir(url);
//            }
//    );
//    $("#servicios").click(
//            function(e)
//            {
//                e.preventDefault();
//                var url = "5f.html?idCentroComercial=$1";
//                url = url.replace("$1", idCentroComercial);
//                redirigir(url);
//            }
//    );
//    $("#masInformacion2").click(
//            function(e)
//            {
//                e.preventDefault();
////                var url = $(this).attr("href");
//                var url=$(this).find("a").attr("href");
//                console.log("url: " + url);
//                openUrl(url);
//            }
//    );

});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getInfoCentroComercial(idCentroComercial)
{
    mostrarDialogo();
    var url = url_base + "centroscomerciales/getInformacionCentroComercial.xml";
    var datos = {
        idCentroComercial: idCentroComercial
    };
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    $("datos", xml).each(function()
                    {
                        var obj = $(this).find("Centroscomerciale");
                        var logo, ciudad, horario, descripcion, url, direccion;
                        logo = $("logo", obj).text();
                        descripcion = $("descripcion", obj).text();
                        url = $("url", obj).text();
                        horario = $("horario", obj).text();
                        direccion = $("direccion", obj).text();
                        obj = $(this).find("Ciudade");
                        ciudad = $("nombre", obj).text();
                        if (logo)
                        {
//                            $("#logo2").attr("src", url_base + "img/centroscomerciales/" + logo);
                            $("#logo2").css("display", "none");
                            $("#logoTienda").css("background-image", "url('"+ url_base + "img/centroscomerciales/" + logo+"')");
                        }
                        console.log("ciudad: "+ciudad);
                        $("#ciudad").html(ciudad);
                        $("#horario").text(horario);
                        $("#direccion").text(direccion);
                        $("#descripcion2").html(descripcion);
                        $("#masInformacion").attr("href", url);

                        $("#tiendas").attr("href", "5g.html?idCentroComercial=" + idCentroComercial);
                        $("#comoLlegar").attr("href", "5b.html?idCentroComercial=" + idCentroComercial);
                        $("#mapas").attr("href", "5d.html?idCentroComercial=" + idCentroComercial);
                        $("#servicios").attr("href", "5f.html?idCentroComercial=" + idCentroComercial);

                    });
                }
                ocultarDialogo();
            });


}