/*Esta pagina se encarga de mostrar el banner seleccionado */

$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                
                var parametros = getUrlVars();
                getNoticia(parametros["idNoticia"]);
            })();
            $("#promocion").click(function(e)
            {
               e.preventDefault(); 
                openUrl($(this).attr("url"));
            });
        }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getNoticia(idNoticia)
{
    mostrarDialogo();
    var estaVacio = true;
    var url = url_base + "noticias/getnoticia.xml";
    var datos = {
        idNoticia: idNoticia
    };
    var xml = ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("datos", xml).each(function()
            {
                var obj = $(this).find("Noticia");
                var src,url,info;
                src = $("banner", obj).text();
                url = $("url", obj).text();
                info = $("informacion", obj).text();
                if (src)
                {
                    estaVacio = false;

                    $("#promocion").attr("src", src);
                    $("#promocion").attr("url", url);
                }
                ocultarDialogo();
                if (estaVacio)
                {
                    $("#Imagen").html("Lo sentimos, no conseguimos informacion");
                }

            });
        }
    });


}