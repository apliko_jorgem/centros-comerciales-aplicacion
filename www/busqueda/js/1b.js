/*
 Esta pagina permite buscar centros comerciales por ciudad, o nombre
 */
var idPais, nombrePais;
var idCiudad, nombreCiudad;
function onDeviceReady()
{


}
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function()
{
    (function()
    {
//Borro el texto que este en el campo del nombre del local
        $("#Buscar").val("");


        autocompletar();
        var parametros = getUrlVars();
        $("#"+parametros["div"]).css("display","block");
        if (parametros["idPais"])
        {
            idPais = parametros["idPais"];
            nombrePais = parametros["nombrePais"];
            nombrePais = cambiarAcentos(nombrePais);
            nombrePais = cambiarAcentos2(nombrePais);
            $("#lnkPais").html(nombrePais);
            $("#lnkCiudad").attr("href", "ciudades.html?url=1b.html&idPais=" + idPais + "&nombrePais=" + nombrePais+"&div=Botonera");
        } else if (getIdPais())
        {
            idPais = getIdPais();
            nombrePais = getNombrePais();
            $("#lnkPais").html(nombrePais);
            $("#lnkCiudad").attr("href", "ciudades.html?url=1b.html&idPais=" + idPais + "&nombrePais=" + nombrePais+"&div=Botonera");
        }
        if (parametros["idCiudad"]) {
            idCiudad = parametros["idCiudad"];
            nombreCiudad = parametros["nombreCiudad"];
            nombreCiudad = cambiarAcentos(nombreCiudad);
            nombreCiudad = cambiarAcentos2(nombreCiudad);
            $("#lnkCiudad").html(nombreCiudad);
        } else if (getIdPais() && getIdCiudad()) {
            idCiudad = getIdCiudad();
            $("#lnkCiudad").html(getNombreCiudad());
        }
        getBanner("1b", "null", "null", "null", "");

    })();
    $("#Slogan2").click(
            function(e)
            {
                e.preventDefault();
                $("#Botonera").toggle();
                if ($(this).css("display") == "block")
                    $("#EscribeAqui").css("display", "none");
            });
    $("#Slogan3").click(
            function(e)
            {
                e.preventDefault();
                $("#EscribeAqui").toggle();
                if ($(this).css("display") == "block")
                    $("#Botonera").css("display", "none");
            });
    $(".menu").on("click", "li",
            function(e)
            {
                e.preventDefault();
                var url = $(this).find("a").attr("href");
                redirigir(url);
            });
    $(".ciudad, #lnkCiudad").click(function(e)
    {
        e.preventDefault();
        if (idPais)
            return;
        else {
            $("#element_to_pop_up").css("background", "rgba(255,255,255,1)");
            $("#element_to_pop_up").css("width", "auto");
            $("#element_to_pop_up").css("height", "auto");
            $("#contenidoPopUp").html("<p>Por favor selecciona un pa&iacute;s </p>");
            $("#btnAceptar").css("display", "inline");
//            $("#btnCancelar").css("display", "inline");

            $('#element_to_pop_up').bPopup();
        }

    });
    $("#Buscar").focus(function()
    {
        console.log("Obtuve foco");
        var scro = $('#Buscar').scrollTop();
        console.log("Scroll: " + scro);
        scro += 1000;
        console.log("Scroll: " + scro);
        $('html, body').stop().animate({
            scrollTop: $($('.nivoSlider')).offset().top
        }, 500);
    });
    /*Esta funcion se encarga de caputara cuando el usario le da clic al boton buscar*/
    $("#btnBuscar").click(
            function(e)
            {
                e.preventDefault();
                if (idCiudad)
                {
                    redirigir("2c.html?idCiudad=" + idCiudad);
                } else {
                    $("#element_to_pop_up").css("background", "rgba(255,255,255,1)");
                    $("#element_to_pop_up").css("width", "auto");
                    $("#element_to_pop_up").css("height", "auto");
                    $("#contenidoPopUp").html("<p>Por favor selecciona un pa&iacute;s </p>");
                    $("#btnAceptar").css("display", "inline");
//            $("#btnCancelar").css("display", "inline");

                    $('#element_to_pop_up').bPopup();
                }


            }
    );
    $("#btnAceptar").click(
            function(e)
            {
                e.preventDefault();
                $('#element_to_pop_up').bPopup().close();
                $("#element_to_pop_up").css("background-color", "rgba(255,255,255,0);");
                $("#contenidoPopUp").html('<img src="../images/loading.gif"  width="90px" height="55px" />  ');
                $("#btnAceptar").css("display", "none");
                $("#btnCancelar").css("display", "none");

            });
    $("#btnCancelar").click(
            function(e)
            {
                e.preventDefault();
                $("#element_to_pop_up").css("background-color", "rgba(255,255,255,0);");
                $("#contenidoPopUp").html('<img src="../images/loading.gif"  width="90px" height="55px" />  ');
                $("#btnAceptar").css("display", "none");
                $("#btnCancelar").css("display", "none");
                $('#element_to_pop_up').bPopup().close();
            });
});


/*Esta funcion se encarga de obtener los nombre de los centros comerciales para que se pueda ver la funcion de autocompletar*/
function autocompletar()
{
    var locales = new Array();
    var url = url_base + "centroscomerciales/index.xml";
    var datos = {
    };
    ajax(url, datos, function(xml)
    {
        $("datos", xml).each(function()
        {
            var obj = $(this).find("Centroscomerciale");

            var idL, nombreL;
            idL = $("id", obj).text();
            nombreL = $("nombre", obj).text();
            nombreL = cambiarAcentos(nombreL);
            nombreL = cambiarAcentos2(nombreL);
            locales.push({id: idL, value: nombreL});
        });
    });
    $("#Buscar").autocomplete({
        source: locales,
        select: function(event, ui)
        {
            //Envio al usuario a la vista 3
            redirigir("5a.html?idCentroComercial=" + ui.item.id);
        },
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = {value: "", label: "Sin coincidencia"};
                ui.content.push(noResult);
            }
        }
    });
}
