var idPais, nombrePais;
var idCiudad, nombreCiudad;

function onDeviceReady() {

}
;
//document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function()
{
    (function()
    {
        var parametros = getUrlVars();
        if (parametros["idPais"])
        {
            idPais = parametros["idPais"];
            nombrePais = parametros["nombrePais"];
            nombrePais = cambiarAcentos(nombrePais);
            nombrePais = cambiarAcentos2(nombrePais);
            $("#lnkPais").html(nombrePais);
            $("#lnkCiudad").attr("href", "ciudades.html?url=7d.html&idPais=" + idPais + "&nombrePais=" + nombrePais + "&div=null");
            setIdPais(idPais);
            setNombrePais(nombrePais);
//        getCiudades(idPais);
        } else if (getIdPais())
        {
            idPais = getIdPais();
            nombrePais = getNombrePais();
            $("#lnkPais").html(nombrePais);
            $("#lnkCiudad").attr("href", "ciudades.html?url=7d.html&idPais=" + idPais + "&nombrePais=" + nombrePais + "&div=null");
        }
        if (parametros["idCiudad"]) {
            idCiudad = parametros["idCiudad"];
            nombreCiudad = parametros["nombreCiudad"];
            console.log("nombreCiudad: " + nombreCiudad);
            nombreCiudad = cambiarAcentos(nombreCiudad);
            console.log("nombreCiudad: " + nombreCiudad);
//            nombreCiudad = cambiarAcentos2(nombreCiudad);
            console.log("nombreCiudad: " + nombreCiudad);
            $("#lnkCiudad").html(nombreCiudad);
            setIdCiudad(idCiudad);
            setNombreCiudad(nombreCiudad);
        } else if (getIdPais() && getIdCiudad()) {
            idCiudad = getIdCiudad();
            $("#lnkCiudad").html(getNombreCiudad());
        }
    })();
    $(".menu").on("click", "li",
            function(e)
            {
                e.preventDefault();
                var url = $(this).find("a").attr("href");
                redirigir(url);
            });
    $(".ciudad, #lnkCiudad").click(function(e)
    {
        e.preventDefault();
        if (idPais)
            return;
        else {
            $("#element_to_pop_up").css("background", "rgba(255,255,255,1)");
            $("#element_to_pop_up").css("width", "auto");
            $("#element_to_pop_up").css("height", "auto");
            $("#contenidoPopUp").html("<p>Por favor selecciona un pa&iacute;s </p>");
            $("#btnAceptar").css("display", "inline");
//            $("#btnCancelar").css("display", "inline");

            $('#element_to_pop_up').bPopup();
        }

    });
//    $("#paises").change(
//            function()
//            {
//                var idPais = $(this).val();
//                var nombrePais = $("#paises option:selected").text();
//                setIdPais(idPais);
//                setNombrePais(nombrePais);
//                getCiudades(idPais);
//            });
//    $("#ciudades").change(
//            function()
//            {
//                var idCiudad = $(this).val();
//                var nombreCiudad = $("#ciudades option:selected").text();
//                setIdCiudad(idCiudad);
//                setNombreCiudad(nombreCiudad);
//                //getCiudades(idPais);
//            });

    /*Esta funcion se encarga de caputara cuando el usario le da clic al boton buscar*/
    $("#btnGuardar").click(
            function(e)
            {
                e.preventDefault();
                var idCiudad = $("#ciudades").val();
                var nombreCiudad = $("#ciudades option:selected").text();
                setIdCiudad(idCiudad);
                setNombreCiudad(nombreCiudad);
                var idPais = $("#paises").val();
                var nombrePais = $("#paises option:selected").text();
                setIdPais(idPais);
                setNombrePais(nombrePais);
                redirigir("../index.html");
            }
    );
});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
///*Se encarga de obtener las ciudades de la base de datos y mostrarlas en un select*/
//function getCiudades(idPais)
//{
//    mostrarDialogo();
//    console.log("Entre getCiudades");
//    $("#ciudades").html("");
//    var url = url_base + "ciudades/index.xml";
//    var datos = {
//        idPais: idPais
//    };
//    ajax(url, datos, function(xml)
//    {
//        if (xml != null)
//        {
//            $("#ciudades").append("<option value='0'>Seleccione ..</option>");
//            $("datos", xml).each(function()
//            {
//                var obj = $(this).find("Ciudade");
//                var valor, texto;
//                valor = $("id", obj).text();
//                texto = $("nombre", obj).text();
//                var html = "<option value='" + valor + "'>" + texto + "</option>";
//                $("#ciudades").append(html);
//            });
//            ocultarDialogo();
//            //Ahora determino si ya existe una ciudad en sesion
//            var idCiudad = getIdCiudad();
//            if (idCiudad)
//            {
//                console.log("Existe idCiudad en sesion");
//                $("#ciudades").val(idCiudad);
//            }
//        }
//    });
//
//
//}
///*Se encarga de obtener las ciudades de la base de datos y mostrarlas en un select*/
//function getPaises()
//{
//    mostrarDialogo();
//    console.log("Entre getPaises");
//    $("#paises").html("");
//    var url = url_base + "paises/index.xml";
//    var datos = {
//    };
//    ajax(url, datos, function(xml)
//    {
//        if (xml != null)
//        {
//            $("#paises").append("<option value='0'>Seleccione ..</option>");
//            $("datos", xml).each(function()
//            {
//                var obj = $(this).find("Paise");
//                var valor, texto;
//                valor = $("id", obj).text();
//                texto = $("nombre", obj).text();
//                var html = "<option value='" + valor + "'>" + texto + "</option>";
//                $("#paises").append(html);
//            });
//            ocultarDialogo();
//            //Ahora determino si ya existe una ciudad en sesion
//            var idCiudad = getIdPais();
//            if (idCiudad)
//            {
//                console.log("Existe paise en sesion");
//                $("#paises").val(idCiudad);
//            }
//        }
//    });
//
//
//}
