/*Esta pagina se encarga de mostrar informacion de un local en especifico*/
var idCentroComercial, idLocal;

function onDeviceReady()
{
    //hide splash screen



}
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function()
{
    $(function()
    {
        var parametros = getUrlVars();
        idLocal = parametros["idLocal"];
        getBanner("3", "null", "null", idLocal, "../");
        getInfoLocal(idLocal);
    }
    );
    $(".menu").on("click","li",
            function(e)
            {
                e.preventDefault();
                var id=$(this).find("a").attr("id");
                if(id!="masInformacion")
                {
                    var url=$(this).find("a").attr("href");
                    redirigir(url);
                }else{
                    var url=$(this).find("a").attr("href");
                    openUrl(url);
                }
                
            });
    
});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getInfoLocal(idLocal)
{
    mostrarDialogo();
    var estaVacion = true;
    var url = url_base + "almacenes/getinformacionlocal.xml";
    var datos = {
        idLocal: idLocal
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("datos", xml).each(function()
            {
                var obj = $(this).find("Almacene");
                var logo, local, piso, seccion, horario, descripcion, url;
                var nombreCC;
                logo = $("logo", obj).text();
                local = $("local", obj).text();
                seccion = $("seccion", obj).text();
                horario = $("horario", obj).text();
                descripcion = $("descripcion", obj).text();
                url = $("url", obj).text();
                
                obj = $(this).find("Centroscomerciale");
                nombreCC = $("nombre", obj).text();
                idCentroComercial = $("id", obj).text();
                obj = $(this).find("Piso");
                piso = $("nombre", obj).text();
                console.log("logo: "+logo);
                if (logo){
                    $("#logoTienda").css("background-image", "url('"+logo+"')");
                    $("#logo3").css("display","none");
                    
                }
                
                $("#local").text(local);
                $("#piso").text(piso);
                $("#seccion").text(seccion);
                $("#horario").text(horario);
                $("#descripcion2").html(descripcion);
                $("#masInformacion").attr("href", url);
                $("#nombreCC").text(nombreCC);

                //Modifico informacion de los botones, pero no sera visible al usuario
                
                console.log("idCentroComercial: "+idCentroComercial);
                //$("#informacionCentroComercial").attr("idCentro",$("centroscomerciale_id",obj).text());
                $("#promociones").attr("href","6b.html?vista=null&idCentroComercial=null&idCategoria=null&idAlmacen="+idLocal);
                $("#comoLlegar").attr("href","4a.html?idCentroComercial=" + idCentroComercial + "&idLocal=" + idLocal);
                $("#informacionCentroComercial").attr("href","5a.html?idCentroComercial=" + idCentroComercial);
            });
        }
        ocultarDialogo();
        if (estaVacio)
            $("#centroscomerciales").append("Lo sentimos, no encontramos informacion");

    });


}