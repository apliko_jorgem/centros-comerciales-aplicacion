/* This code is used to run as soon as Intel activates */
function onDeviceReady()
{

}
;
document.addEventListener("deviceready", onDeviceReady, false);


$(document).ready(function()
{
    /*Funciones autoejecutables*/
    (function()
    {
        //hide splash screen
        console.log("ingrese");
        var parametros = getUrlVars();
        //Determino si llego el id de la ciudad
        if (parametros["idCiudad"])
        {
            var idCiudad, nombreCiudad, urlD,div;
            idCiudad = parametros["idCiudad"];
            nombreCiudad = parametros["nombreCiudad"];
            nombreCiudad = cambiarAcentos(nombreCiudad);
            urlD = parametros["url"];
            div = parametros["div"];
            
            getCentrosComercialesByCiudad(urlD, idCiudad, nombreCiudad,div);
        }
    })();
    $(".menu").on("click","li",
            function(e)
            {
                e.preventDefault();
                var url=$(this).find("a").attr("href");
                redirigir(url);
            });
});

//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    console.log("Cerrando");
//    $('#element_to_pop_up').bPopup().close();
//}

function getCentrosComercialesByCiudad(urlD, idCiudad, nombreCiudad,div)
{
    mostrarDialogo();
    estaVacio = true;
    console.log("Entre con nombreCiudad: " + nombreCiudad);
    var url = url_base + "centroscomerciales/getcentroscomercialesbyciudad.xml";
    var datos = {
        idCiudad: idCiudad
    };
    var xml = ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    var html = "";
//                    var html = "<li class='todos'>" +
//                            "<a href='$1?idCiudad=$2&nombreCiudad=$3&idCentroComercial=$4&nombreCentroComercial=$5' id='lnktodos'>$5</a>" +
//                            "<a class='go'></a>" +
//                            "</li>";
                    html = html.replace("$1", urlD);
                    html = html.replace("$2", idCiudad);
                    html = html.replace("$3", nombreCiudad);
                    html = html.replace("$4", 0);
                    html = html.replace("$5", "Todos");
                    html = html.replace("$5", "Todos");
                    $("#centroscomerciales").append(html);
                    $("datos", xml).each(function()
                    {
                        var obj = $(this).find("Centroscomerciale");
                        var valor, texto;

                        valor = $("id", obj).text();
                        texto = $("nombre", obj).text();
                        console.log("valor: " + valor);
                        if (valor)
                        {
                            estaVacio = false;
                            var html = "<li class='todos'>" +
                                    "<a href='$1?idCiudad=$2&nombreCiudad=$3&idCentroComercial=$4&nombreCentroComercial=$5&div=$6' id='lnktodos'>$5</a>" +
                                    "<a class='go'></a>" +
                                    "</li>";
                            html = html.replace("$1", urlD);
                            html = html.replace("$2", idCiudad);
                            html = html.replace("$3", nombreCiudad);
                            html = html.replace("$4", valor);
                            html = html.replace("$5", texto);
                            html = html.replace("$5", texto);
                            html = html.replace("$6", div);
                            $("#centroscomerciales").append(html);
                        }

                    });
                }
                ocultarDialogo();
                if (estaVacio)
                    $("#centroscomerciales").html("Lo sentimos, no encontramos informacion");

            });

}