var idCentroComercial, piso;
/*Esta pagina se de mostrar una mapa de un piso en especifico */
function onDeviceReady() {
    //hide splash screen



}
;
document.addEventListener("deviceready", onDeviceReady, false);
$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                var parametros = getUrlVars();

                getMapa(parametros["idPiso"]);
            })();
            $("#banos").click(
                    function(e)
                    {
                        getServicio(idCentroComercial, "1", piso);
                    });
            $("#cajero").click(
                    function(e)
                    {
                        getServicio(idCentroComercial, "2", piso);
                    });
            $("#ascensor").click(
                    function(e)
                    {
                        getServicio(idCentroComercial, "3", piso);
                    });
            $("#parqueo").click(
                    function(e)
                    {
                        getServicio(idCentroComercial, "4", piso);
                    });
            $("#transporte").click(
                    function(e)
                    {
                        getServicio(idCentroComercial, "5", piso);
                    });
            $("#informacion").click(
                    function(e)
                    {
                        getServicio(idCentroComercial, "6", piso);
                    });
            $("#cerrar").click(
                    function(e)
                    {
                        e.preventDefault();
                        var url = "5d.html?idCentroComercial=" + idCentroComercial;
                        redirigir(url);
                    }
            );

        }


);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getMapa(idPiso)
{
    mostrarDialogo();
    var estaVacio = true;
    var url = url_base + "pisos/getMapaByPiso.xml";
    var datos = {
        idPiso: idPiso
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {

            log("5e", "getMapa", "Entre al if");
            $("datos", xml).each(function()
            {

                var obj = $(this).find("Piso");
                var img;
                img = url_base + "img/pisos/";
                img += $("mapa", obj).text();
                piso = $("numero", obj).text();
                if (img)
                {
                    estaVacio = false;
//                                $("#mapa2").attr("src",img);
                    ini(img);
                    obj = $(this).find("Centroscomerciale");
                    idCentroComercial = $("id", obj).text();
                    getBanner("5e", idCentroComercial, "null", "null", "../");
                }

            });
        }
        ocultarDialogo();
        if (estaVacio)
        {
            $("#mapa").html("Lo sentimos, no conseguimos informacion");
        }
    });


}
function ini(urlM)
{
    console.log("Urlm: " + urlM);
    map = new OpenLayers.Map('Imagenmapa');

    var graphic = new OpenLayers.Layer.Image(
            'Piso',
            urlM,
            new OpenLayers.Bounds(0, 0, 1000, 1000),
            new OpenLayers.Size(300, 300),
            {numZoomLevels: 10}
    );

    map.addLayer(graphic);
    markers = new OpenLayers.Layer.Markers("Marcadores");
    map.addLayer(markers);
    var center = new OpenLayers.LonLat(500, 500);
    map.setCenter(center, 1);

}


function agregarMarcador(longitud, latitud, mensajeHtml, closeBox, overflow, icono)
{
    var size = new OpenLayers.Size(32, 32);
    var icon = new OpenLayers.Icon(icono,size);
    markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(longitud, latitud), icon));
}
/* Se encarga de obtener las coordenadas de los servicios de un piso especifico de un centro comercial
 * idCentroComercial    --> Centro comercial donde se consultaran los servicios
 * idServicio           --> Servicio que se va a consultar
 * Piso                 -->
 */
function getServicio(idCentroComercial, idServicio, piso)
{
    mostrarDialogo();
    var url = url_base + "centroscomercialesservicios/getservicios.xml";
    var datos = {
        idCentroComercial: idCentroComercial,
        idServicio: idServicio,
        piso: piso
    }
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    $("datos", xml).each(
                            function()
                            {
                                var obj = $(this).find("CentroscomercialesServicio");
                                var lat = $("lat", obj).text();
                                var lon = $("lon", obj).text();
                                markers = new OpenLayers.Layer.Markers("Marcadores");
                                map.addLayer(markers);
                                switch (idServicio)
                                {
                                    case "1":
                                        agregarMarcador(lat, lon, "10", null, null, "../images/btbanos2.png");
//                                        agregarMarcador(lat, lon, "10", null, null, "../images/beadlightgreen/button.png");
                                        break;
                                    case "2":
                                        agregarMarcador(lat, lon, "10", null, null, "../images/btCajero2.png");
                                        break;
                                    case "3":
                                        agregarMarcador(lat, lon, "10", null, null, "../images/btascensor2.png");
                                        break;
                                    case "4":
                                        agregarMarcador(lat, lon, "10", null, null, "../images/btparqueo2.png");
                                        break;
                                    case "5":
                                        agregarMarcador(lat, lon, "10", null, null, "../images/btTransporte2.png");
                                        break;
                                    case "6":
                                        agregarMarcador(lat, lon, "10", null, null, "../images/btInformacion2.png");
                                        break;
                                }
                                
                            });
                }
                ocultarDialogo();
            });
}