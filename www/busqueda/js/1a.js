//Esta pagina permite buscar almacenes dentro  de un centro comercial, tanto por el nombre, o por la categoria
var parametros = getUrlVars();
var idPais, nombrePais;
var idCiudad, nombreCiudad;
var idCentro, nombreCentro;
var idCategoria, nombreCategoria;

//Variable que me dice si puedo o no usar el boton de buscar
var puede = false;
/* This code is used to run as soon as Intel activates */
function onDeviceReady()
{


}
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function()
{
    /*Funciones autoejecutables*/
    (function()
    {
        //Borro el texto que este en el campo del nombre del local
        $("#Buscar").val("");
        //Determino cual div se debe mostrar
        $("#"+parametros["div"]).css("display","block");
        
        autocompletar();
        //Determino si llego el id de la ciudad
        if (parametros["idPais"])
        {

            idPais = parametros["idPais"];
            nombrePais = parametros["nombrePais"];
            nombrePais = cambiarAcentos(nombrePais);
            nombrePais = cambiarAcentos2(nombrePais);
            //Modifico el texto que dice le nombre de la ciudad
            $("#lnkPais").text(nombrePais);
            //Modifico y habilito el link para buscar centros comerciales
            $("#lnkCiudad").attr("href", "ciudades.html?url=1a.html&idPais=" + idPais + "&nombrePais=" + nombrePais+"&div=Botonera");
        } else if (getIdPais())
        {
            idPais = getIdPais();
            nombrePais = getNombrePais();
            nombrePais = cambiarAcentos(nombrePais);
            nombrePais = cambiarAcentos2(nombrePais);
            //Modifico el texto que dice le nombre del Pais
            $("#lnkPais").text(nombrePais);
            //Modifico y habilito el link para buscar Ciudades
            $("#lnkCiudad").attr("href", "ciudades.html?url=1a.html&idPais=" + idPais + "&nombrePais=" + nombrePais+"&div=Botonera");
        }
        if (parametros["idCiudad"])
        {

            idCiudad = parametros["idCiudad"];
            nombreCiudad = parametros["nombreCiudad"];
            nombreCiudad = cambiarAcentos(nombreCiudad);
            nombreCiudad = cambiarAcentos2(nombreCiudad);
            //Modifico el texto que dice le nombre de la ciudad
            $("#lnkCiudad").text(nombreCiudad);
            //Modifico y habilito el link para buscar centros comerciales
            $("#lnkCentroComercial").attr("href", "centroscomerciales.html?url=1a.html&idCiudad=" + idCiudad + "&nombreCiudad=" + nombreCiudad+"&div=Botonera");
        } else if (getIdCiudad() && getIdPais())
        {
            idCiudad = getIdCiudad();
            nombreCiudad = getNombreCiudad();
            nombreCiudad = cambiarAcentos(nombreCiudad);
            nombreCiudad = cambiarAcentos2(nombreCiudad);
            //Modifico el texto que dice le nombre de la ciudad
            $("#lnkCiudad").text(nombreCiudad);
            //Modifico y habilito el link para buscar centros comerciales
            $("#lnkCentroComercial").attr("href", "centroscomerciales.html?url=1a.html&idCiudad=" + idCiudad + "&nombreCiudad=" + nombreCiudad+"&div=Botonera");
        }
        if (parametros["idCentroComercial"])
        {
            console.log("Llego un id de cc");
            idCentro = parametros["idCentroComercial"];
            nombreCentro = parametros["nombreCentroComercial"];
            nombreCentro = cambiarAcentos(nombreCentro);
            nombreCentro = cambiarAcentos2(nombreCentro);
            //Modifico el texto que dice le nombre del centro comercial
            $("#lnkCentroComercial").text(nombreCentro);
            //Modifico y habilito el link para buscar categorias
            $("#lnkCategorias").attr("href", "categorias.html?url=1a.html&idCiudad=" + idCiudad + "&nombreCiudad=" + nombreCiudad + "&idCentroComercial=" + idCentro + "&nombreCentroComercial=" + nombreCentro+"&div=Botonera");
            idCategoria = 0;
            puede = true;
        }
        if (parametros["idCategoria"])
        {

            idCategoria = parametros["idCategoria"];
            nombreCategoria = parametros["nombreCategoria"];
            nombreCategoria = cambiarAcentos(nombreCategoria);
            nombreCategoria = cambiarAcentos2(nombreCategoria);

            //Modifico el texto que dice le nombre del centro comercial
            $("#lnkCategorias").text(nombreCategoria);
            puede = true;

        } else {

        }
//    $('#slider').nivoSlider();
        getBanner("1a", "null", "null", "null", "../");
        
    })();

    $(".menu").on("click", "li",
            function(e)
            {
                e.preventDefault();
                var url = $(this).find("a").attr("href");
                redirigir(url);
            });
    $(".ciudad, #lnkCiudad").click(function(e)
    {
        e.preventDefault();
        if (idPais)
            return;
        else {
            mostrarMensaje("<p>Por favor selecciona un pa&iacute;s </p>");
        }

    });
    $(".cc, #lnkCentroComercial").click(function(e)
    {
        e.preventDefault();
        if (idCiudad)
            return;
        else {
            mostrarMensaje("<p>Por favor selecciona una ciudad </p>");
        }

    });
    $(".cc, #lnkCentroComercial").click(function(e)
    {
        e.preventDefault();
        if (idCiudad)
            return;
        else {
            mostrarMensaje("<p>Por favor selecciona una ciudad </p>");
        }

    });
    $(".categoria, #lnkCategorias").click(function(e)
    {
        e.preventDefault();
        if (idCentro)
            return;
        else {
            mostrarMensaje("<p>Por favor selecciona un centro comercial </p>");
        }

    });
    $("#Slogan2").click(
            function(e)
            {
                e.preventDefault();
                $("#Botonera").toggle();
                if ($(this).css("display") == "block")
                    $("#EscribeAqui").css("display", "none");
            });
    $("#Slogan3").click(
            function(e)
            {
                e.preventDefault();
                $("#EscribeAqui").toggle();
                if ($(this).css("display") == "block")
                    $("#Botonera").css("display", "none");
            });
    
    $("#btnBuscar").click(
            function(e)
            {
                e.preventDefault();
                if (puede)
                {
                    var url = "2a.html?idCiudad=$1&idCentroComercial=$2&idCategoria=$3";
                    url = url.replace("$1", idCiudad);
                    url = url.replace("$2", idCentro);
                    url = url.replace("$3", idCategoria);
                    redirigir(url);
                } else {
                    mostrarMensaje("<p>Por favor selecciona todas las opciones </p>");
                }
            }
    );
    $("#Buscar").focus(function()
    {
        console.log("Obtuve foco");
        var scro = $('#Buscar').scrollTop();
        console.log("Scroll: " + scro);
        scro += 1000;
        console.log("Scroll: " + scro);
        $('html, body').stop().animate({
            scrollTop: $($('.nivoSlider')).offset().top
        }, 500);
    });
});

/*Esta funcion va  a la vista 2a y le envia los parametros necesarios*/
function goTo2a()
{
    //Obtengo las variables a enviar
    var idCiudad, idCentroComercial, idCategoria;
    idCiudad = $("#ciudades").val();
    idCentroComercial = $("#centroscomerciales").val();
    idCategoria = $("#categorias").val();

    //Redirijo
    $(location).attr("href", "2a.html?idCiudad=" + idCiudad + "&idCentroComercial=" + idCentroComercial + "&idCategoria=" + idCategoria);
}
/*Esta funcion se encarga de obtener los nombre de los locales para que se pueda ver la funcion de autocompletar*/
function autocompletar()
{
    mostrarDialogo();
    var locales = new Array();
    var url = url_base + "almacenes/getlocales.xml";
    var datos = {
    };

    ajax(url, datos, function(xml)
    {
        $("datos", xml).each(function()
        {
            var obj = $(this).find("Almacene");

            var idL, nombreL;
            idL = $("id", obj).text();
            nombreL = $("nombre", obj).text();
            nombreL = cambiarAcentos(nombreL);
            nombreL = cambiarAcentos2(nombreL);
            locales.push({id: idL, value: nombreL});
        });
        ocultarDialogo();
    });





    $("#Buscar").autocomplete({
//        source: function(request, response) {
//            var matches = $.map(locales, function(acItem) {
//                console.log("Texto: " + acItem);
//                if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
//                    return acItem;
//                }
//            });
//            response(matches);
//        },
        source: locales,
        select: function(event, ui)
        {
            //Envio al usuario a la vista 3
            var url = "2b.html?idLocal=$1";
            url = url.replace("$1", ui.item.id);
            redirigir(url);
        },
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = {value: "", label: "Sin coincidencia"};
                ui.content.push(noResult);
            }
        }
    });
}
/*Esta funcion va  a la vista 2b y le envia los parametros necesarios*/
function goTo2b()
{
    //Obtengo las variables a enviar
    var nombreLocal;
    nombreLocal = $("#Buscar").val();

    //Redirijo
    $(location).attr("href", "2b.html?nombreLocal=" + nombreLocal);
}
            