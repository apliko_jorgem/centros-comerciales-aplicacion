/*Esta pagina se encarga de mostrar los servicios del centro comercial */
function onDeviceReady(){
    //hide splash screen
    
    
    var parametros=getUrlVars();
    getBanner("5f",parametros["idCentroComercial"],"null","null","../");
    getServicios(parametros["idCentroComercial"]);
};
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(
    function()
    {
        /*Funciones autoejecutables*/
        (function()
         {
             
         })();
    }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getServicios(idCentroComercial)
{
    mostrarDialogo();
    var estaVacio=true;
    var url=url_base+"centroscomerciales/getInformacionCentroComercial.xml";
    var datos={
        idCentroComercial:idCentroComercial
    };
    var xml=ajax(url,datos,function(xml)
                 {
                    if(xml!=null)
                    {
                        $("datos",xml).each(function()
                        {
                            
                            var obj=$(this).find("Centroscomerciale");
                            var nombre;
                            nombre=$("servicios",obj).text();
                            if(nombre)
                            {
                                estaVacio=false;
                                var html="$1<br>";
                                html=html.replace("$1",nombre);
                                $("#servicios").html(html);
                            }
                            
                        });
                    }
                     ocultarDialogo();
                     if(estaVacio)
                     {
                         $("#servicios").html("Lo sentimos, no conseguimos infformacion");
                     }
                 });
    
    
}