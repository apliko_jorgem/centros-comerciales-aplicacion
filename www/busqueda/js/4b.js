//Esta pagina se encarga de obtener los ids de los locales para graficarlos en el mapa
var map, markers;
var idCentroComercial;
var idO1, idF1;
var idLocal;

/*Posiciones de los locales donde:
 x=      Posicion x
 y=      Posicion y
 idE=    Id de la escalera
 bE=     Variable que representa el bloque al que pertenece la escalera
 ex=     Posicion escalera x
 ey=     Posicion escalera y
 p=      Piso del local
 urlM=   Url de la imagen del mapa
 */
var x0, y0, idE0, bE0, ex0, ey0, p0, urlM;
var x1, y1, ex1, ey1, p1;

function onDeviceReady()
{
    //hide splash screen


//      var parametros=getUrlVars();
//      //getBanner("4b","null","null",parametros["idO"],"null");
//      mostrarDialogo();
//      obtenerCoordenadas(parametros["idO"],parametros["idF"],function()
//                         {
//                             ini(urlM);
//                             obtemerCoordenadas2(parametros["idF"],function()
//                                                 {
//                                                     analisis();
//                                                     ocultarDialogo();
//                                                 });
//                         });
}
;
document.addEventListener("deviceready", onDeviceReady, false);
$(document).ready(function()
{
    (function()
    {
//        $('#inform').jqrotate(35);
        console.log("entre");
        var parametros = getUrlVars();
        //getBanner("4b","null","null",parametros["idO"],"null");
        mostrarDialogo();
        idLocal = parametros["idF"];
        obtenerCoordenadas(parametros["idO"], parametros["idF"], function()
        {
            ini(urlM);
            obtemerCoordenadas2(parametros["idF"], function()
            {
                analisis();
                ocultarDialogo();
                getBanner("4b", "null", "null", parametros["idO"], "../");
            });
        });
    }
    )();
//    $("#banos").click(
//            function(e)
//            {
//                getServicio(idCentroComercial, "1", p0);
//            });
//    $("#cajero").click(
//            function(e)
//            {
//                getServicio(idCentroComercial, "2", p0);
//            });
//    $("#ascensor").click(
//            function(e)
//            {
//                getServicio(idCentroComercial, "3", p0);
//            });
//    $("#parqueo").click(
//            function(e)
//            {
//                getServicio(idCentroComercial, "4", p0);
//            });
//    $("#transporte").click(
//            function(e)
//            {
//                getServicio(idCentroComercial, "5", p0);
//            });
//    $("#informacion").click(
//            function(e)
//            {
//                getServicio(idCentroComercial, "6", p0);
//            });

    $("#continuar").click(
            function(e)
            {
                e.preventDefault();
                obtenerCoordenadas(idO1, idF1, function()
                {
                    obtemerCoordenadas2(idF1, function()
                    {
                        analisis();
                    });
                });
            }
    );
    $("#cerrar").click(
            function(e)
            {
                e.preventDefault();
                var url = "3.html?idLocal=" + idLocal;
                redirigir(url);
            }
    );

});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
/*Esta funcion se encarga de obtener las coordenadas de los locales dados*/
function  obtenerCoordenadas(idO, idF, callback)
{
    //alert("entre con " + idO + " y " + idF);
    idF1 = idF;

    /*Obtencion de datos*/

    var url = url_base + "almacenes/getinformacionlocal.xml";
    var datos = {
        idLocal: idO
    }
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            console.log("Ente al primer ajax");
            $("datos", xml).each(
                    function()
                    {
                        //Obtengo el id del centro comercial
                        var obj = $(this).find("Centroscomerciale");
                        idCentroComercial = $("id", obj).text();
                        obj = $(this).find("Piso");
                        urlM = url_base + "img/pisos/";
                        urlM += $("mapa", obj).text();
                        p0 = $("numero", obj).text();

                        obj = $(this).find("Almacene");
                        x0 = $("x", obj).text();
                        y0 = $("y", obj).text();
                        idE0 = $("escaleracercana", obj).text();
                        bE0 = $("bloque", obj).text();

                        //Obtengo las coordenadas de la escalera
                        url = url_base + "almacenes/getcoordenadasescalera.xml";
                        datos = {
                            idEscalera: idE0
                        }
                        ajax(url, datos,
                                function(xml2)
                                {
                                    console.log("Ente al segundo ajax");
                                    $("datos", xml2).each(
                                            function()
                                            {
                                                var obj2 = $(this).find("Almacene");
                                                ex0 = $("x", obj2).text();
                                                ey0 = $("y", obj2).text();

                                            });

                                });

                    }
            );
            callback();

            /*Fin Obtencion de datos*/
        } else {
        }
    });



}

function obtemerCoordenadas2(idF, callback)
{
    var url = url_base + "almacenes/getinformacionlocal.xml";
    var datos = {
        idLocal: idF
    }
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    console.log("Ente al tercer ajax");
                    $("datos", xml).each(
                            function()
                            {
                                var obj = $(this).find("Almacene");
                                x1 = $("x", obj).text();
                                y1 = $("y", obj).text();
                                obj = $(this).find("Piso");
                                p1 = $("numero", obj).text();




                            });
                }
                callback();
            });

}

function analisis()
{
    /*Analisis Ruta*/
    console.log(p0 + "==" + p1);
    /*Si se encuentran en el mismo piso*/
//    alert("Paso "+p0+ " de "+p1);
    var tex = "Paso " + p0 + " de " + p1;
//    $("#inform").text(tex);
    $("#pasoI").attr("src","../img/"+p0+".png");
    $("#pasoF").attr("src","../img/"+p1+".png");
//    $("#pasoF").text("de " + p1);
    if (p0 == p1)
    {
        agregarMarcador(x0, y0, "10", null, null, "../images/beadlightgreen/button.png");
//        agregarMarcador(x0, y0, "10", null, null, "../images/marcador.png");
        //$("#origen").css("top",y0);
        //$("#origen").css("left",x0);
        agregarMarcador(x1, y1, "10", null, null, "../images/moreblack/button.png");
        //$("#fin").css("top",y1);
        //$("#fin").css("left",x1);
        /* Desabilito el boton continuar */
        $("#continuar").css("display", "none");
    } else {
        log("4b", "obtenerCoordenadas", "Entre al else");
        /*Si no se encuentran en el mismo piso, muestro el mapa que va desde el origen hasta las escaleras*/
        //$("#origen").css("top",y0);
        //$("#origen").css("left",x0);
        agregarMarcador(x0, y0, "10", null, null, "../images/beadlightgreen/button.png");
        //$("#fin").css("top",ey0);
        //$("#fin").css("left",ex0);
        agregarMarcador(ex0, ey0, "10", null, null, "../images/moreblack/button.png");
        /* Habilito el boton continuar */
        $("#continuar").css("display", "block");
        /*Obtengo el id de la escalera del siguiente piso*/
        obtenerEscalera(parseInt(p0) + 1, bE0);
        /*idO1=idE;*/
        /*        idF1=idF;*/


    }
    /*Fin Analisis Ruta*/
}

/*
 Se encarga de obtener el id de la escalera de un piso en especifico
 piso      ->  Piso de la escalera
 bloque    ->  Bloque de la escalera, ya que un piso puede tener multiples escaleras
 */
function obtenerEscalera(piso, bloque)
{
    var id = -1;
    var url = url_base + "almacenes/getescalerabypisobloque.xml";
    var datos = {
        piso: piso,
        bloque: bloque
    }
    ajax(url, datos,
            function(xml)
            {
                console.log("no es nulo el xml");
                console.log("piso: " + piso);
                console.log("bloque: " + bloque);
                if (xml != null)
                {
                    $("datos", xml).each(
                            function()
                            {
                                console.log("encontre un datos");
                                var obj = $(this).find("a");
                                idO1 = $("id", obj).text();
                                console.log("el id es :" + idO1);
                            }
                    );
                } else {
                    id01 - 1;
                }

            });


}

function ini(urlM)
{
    console.log("Urlm: " + urlM);
    map = new OpenLayers.Map('Imagenmapa');

    var graphic = new OpenLayers.Layer.Image(
            'Piso',
            urlM,
            new OpenLayers.Bounds(0, 0, 1000, 1000),
            new OpenLayers.Size(300, 300),
            {numZoomLevels: 10}
    );

    map.addLayer(graphic);
    markers = new OpenLayers.Layer.Markers("Marcadores");
    map.addLayer(markers);
    var center = new OpenLayers.LonLat(500, 500);
    map.setCenter(center, 1);

}


function agregarMarcador(longitud, latitud, mensajeHtml, closeBox, overflow, icono)
{
    var size = new OpenLayers.Size(75, 75);
    var icon = new OpenLayers.Icon(icono, size);
    markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(longitud, latitud), icon));
}

/* Se encarga de obtener las coordenadas de los servicios de un piso especifico de un centro comercial
 * idCentroComercial    --> Centro comercial donde se consultaran los servicios
 * idServicio           --> Servicio que se va a consultar
 * Piso                 -->
 */
function getServicio(idCentroComercial, idServicio, piso)
{
    mostrarDialogo();
    var url = url_base + "centroscomercialesservicios/getservicios.xml";
    var datos = {
        idCentroComercial: idCentroComercial,
        idServicio: idServicio,
        piso: piso
    }
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    $("datos", xml).each(
                            function()
                            {
                                var obj = $(this).find("CentroscomercialesServicio");
                                var lat = $("lat", obj).text();
                                var lon = $("lon", obj).text();
                                markers = new OpenLayers.Layer.Markers("Marcadores");
                                map.addLayer(markers);
                                agregarMarcador(lat, lon, "10", null, null, "../images/btbanosOver.svg");
                            });
                }
                ocultarDialogo();
            });
}