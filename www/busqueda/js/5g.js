/*Esta pagina se encarga de mostrar los loscales del centro comercial */


$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                var parametros = getUrlVars();
                getBanner("5g", parametros["idCentroComercial"], "null", "null", "../");
                getLocales(parametros["idCentroComercial"]);
            })();
            $(".menu").on("click", "li",
                    function(e)
                    {
                        e.preventDefault();
                        var url = $(this).find("a").attr("href");
                        redirigir(url);
                    });
        }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getLocales(idCentroComercial)
{
    console.log("Obteniendo locales");
    mostrarDialogo();
    var estaVacio = true;
    var url = url_base + "almacenes/getlocalesbycentrocomercial.xml";
    var datos = {
        idCentroComercial: idCentroComercial
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("#locales").append("");
            $("datos", xml).each(function()
            {

                var obj = $(this).find("Almacene");
                var nombre, id;
                nombre = $("nombre", obj).text();
                id = $("id", obj).text();
                if (id)
                {
                    estaVacio = false;
                    var html = "<li class='todos'>" +
                            "<a href='3.html?idLocal=$2' id='lnktodos'>$1</a>" +
                            "<a class='go'></a>" +
                            "</li>";
                    html = html.replace("$1", nombre);
                    html = html.replace("$2", id);
                    $("#locales").append(html);
                }

            });
        }
        ocultarDialogo();
        if (estaVacio)
        {
            $("#locales").html("Lo sentimos, no conseguimos informacion");
        }
    });


}