$(document).ready(function()
{
    /*Funciones autoejecutables*/
    (function()
    {
        console.log("entre a la autofuncion");
        var parametros = getUrlVars();
        if (parametros['url'] == "1a.html") {
            $("#titulo").text("ENCUENTRA LA TIENDA QUE BUSCAS");
            $("#BT").attr("src", "../images/btBTImg.svg");
        } else {
            $("#titulo").text("ENCUENTRA EL CENTRO COMERCIAL QUE BUSCAS");
            $("#BT").attr("src", "../images/btBCCImg.svg");
        }
        getPaises(parametros["url"], parametros["div"]);
    })();
    $(".menu").on("click", "li",
            function()
            {
//                e.preventDefault();
                var url = $(this).find("a").attr("href");
//                alert(url);
                redirigir(url);
            });

});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}


function getPaises(urlD, div)
{
    mostrarDialogo();
    estaVacio = true;
    console.log("urlD: " + urlD);
    var url = url_base + "paises/index.xml";
    var datos = {
    };

    console.log("url: " + url);
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
//            $("#ciudades").append("<option value='0'>Seleccione...</option>");
                    $("datos", xml).each(function()
                    {

                        var obj = $(this).find("Paise");
                        var valor, texto;
                        valor = $("id", obj).text();
                        texto = $("nombre", obj).text();
                        if (valor)
                        {
                            console.log("Valor: " + valor);
                            estaVacio = false;
                            var html = "<li class='todos'>" +
                                    "<a href='$1?idPais=$2&nombrePais=$3&div=$5' id='lnktodos'>$4</a>" +
                                    "<a class='go'></a>" +
                                    "</li>";
                            html = html.replace("$1", urlD);
                            html = html.replace("$2", valor);
                            html = html.replace("$3", texto);
                            html = html.replace("$4", texto);
                            html = html.replace("$5", div);
                            $("#paises").append(html);
                        }
                    });

                }
                ocultarDialogo();
                if (estaVacio)
                    $("#ciudades").html("Lo sentimos, no encontramos informacion");

            });


}