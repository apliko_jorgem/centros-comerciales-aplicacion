/*Esta pagina se encarga de mostrar informacion , dependiendo de los datos recibidos */
function onDeviceReady() {
    //hide splash screen



};
//document.addEventListener("deviceready", onDeviceReady, false);
$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                var parametros = getUrlVars();
                getBanner("5c", parametros["idCentroComercial"], "null", "null", "../");
                getInformacion(parametros["idMedioTransporte"], parametros["idCentroComercial"]);
            })();
        }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getInformacion(idMedioTransporte, idCentroComercial)
{
    console.log("Entre a obtener informacion");
    var nombreTransporte="";
    switch (idMedioTransporte)
    {
        case "1":
            $("#imgTransporte").attr("src","../images/pictocarro.svg");
            nombreTransporte="Autom&oacute;vil";
            break;
        case "2":
            $("#imgTransporte").attr("src","../images/bus.svg");
            nombreTransporte="Transporte P&uacute;blico";
            break;
        case "3":
            $("#imgTransporte").attr("src","../images/bici.svg");
            nombreTransporte="Otros";
            break;
    }
    mostrarDialogo();
    var url = url_base + "centroscomerciales_mediostransportes/getinformacionmediotransporte.xml";
    var datos = {
        idMedioTransporte: idMedioTransporte,
        idCentroComercial: idCentroComercial
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            log("el xml no es nulo");
            $("datos", xml).each(function()
            {

                var obj = $(this).find("c_m");
                var nombre, informacion;
                informacion = $("descripcion", obj).text();
                obj = $(this).find("m");
                nombre = $("nombre", obj).text();
                $("#nombre").html(nombre);
                $("#informacion").html(informacion);
                console.log("nombre: " + nombre);
                if (nombre)
                {
                    if (idMedioTransporte == 1)
                    {
                        console.log("entre en el else");

                        obj = $(this).find("cc");
                        var lat, lon;
                        lat = $("lat", obj).text();
                        lon = $("lon", obj).text();
                        activarMapa(lat, lon);

                    }
                } else {
                    $("#nombre").html(nombreTransporte);
                    $("#informacion").html("Lo sentimos, no encontramos informaci&oacute;n");
                }



            });
        }
        ocultarDialogo();
    });


}

function activarMapa(lat, lon)
{
    console.log("entre en el mapa");
    console.log("lat: " + lat);
    console.log("lon: " + lon);
    $("#map_canvas").css("display", "block");
    var mapCanvas = document.getElementById('map_canvas');
    var mapOptions = {
        center: new google.maps.LatLng(lat, lon),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);

    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lon),
        map: map,
        icon: iconBase + 'schools_maps.png'
    });




}