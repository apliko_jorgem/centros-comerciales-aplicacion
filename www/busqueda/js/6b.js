/*Esta pagina se encarga de mostrar las promociones */
function onDeviceReady() {
    //hide splash screen


}
;
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                var parametros = getUrlVars();
                getPromociones(parametros["vista"], parametros["idCentroComercial"], parametros["idCategoria"], parametros["idAlmacen"]);
            })();
        }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getPromociones(vista, idCentroComercial, idCategoria, idAlmacen)
{
    if (!vista)
        vista = "null";
    if (!idCentroComercial)
        idCentroComercial = "null";
    if (!idCategoria)
        idCategoria = "null";
    if (!idAlmacen)
        idAlmacen = "null";
    mostrarDialogo();
    var estaVacio = true;
//    var url = url_base + "promociones/getpromociones.xml";
    var url = url_base + "banners/getbanners.xml";
    $("#promociones").append("");
    var datos = {
        vista: vista,
        idCentroComercial: idCentroComercial,
        idCategoria: idCategoria,
        idAlmacen: idAlmacen
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("datos", xml).each(function()
            {
                var obj = $(this).find("Banner");

                var src, id;
                src = $("imagen", obj).text();
                id = $("id", obj).text();
                if (id)
                {
                    estaVacio = false;
                    var html = "<a href='$2' style='width:100%;'><img src='$1'  /></a><br><br><br>";
                    html = html.replace("$1", src);
                    html = html.replace("$2", "6a.html?idPromocion=" + id);
                    $("#promociones").append(html);
                }

            });
        }
        ocultarDialogo();
        if (estaVacio)
        {
            $("#promociones").html("Lo sentimos, no conseguimos informacion");
        }
    });


}