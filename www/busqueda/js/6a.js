/*Esta pagina se encarga de mostrar el banner seleccionado */
function onDeviceReady() {
//hide splash screen


}
;
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                var parametros = getUrlVars();
                getPromociones(parametros["idPromocion"]);
            })();
            $("#promocion").click(function(e)
            {
               e.preventDefault(); 
                openUrl($(this).attr("link"));
            });
            $("#lnkMasInformacion").click(function(e)
            {
               e.preventDefault(); 
                openUrl($(this).attr("href"));
            });
        }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getPromociones(idPromocion)
{
    mostrarDialogo();
    console.log("idPromocion: " + idPromocion);
    var estaVacio = true;
    var url = url_base + "banners/getbannerbyid.xml";
    var datos = {
        idPromocion: idPromocion
    };
    var xml = ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("datos", xml).each(function()
            {
                var obj = $(this).find("Banner");
                var src,link,info;
                src = $("imagen", obj).text();
                link = $("link", obj).text();
                info = $("informacion", obj).text();
                if (src)
                {
                    estaVacio = false;

                    $("#promocion").attr("src", src);
                    $("#promocion").attr("link", link);
                    $("#lnkMasInformacion").attr("href", link);
                    $("#informacion").text(info);
                    console.log("link: "+link);
                }
                ocultarDialogo();
                if (estaVacio)
                {
                    $("#Imagen").html("Lo sentimos, no conseguimos informacion");
                }

            });
        }
    });


}