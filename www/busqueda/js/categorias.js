
function onDeviceReady()
{
    //hide splash screen




}
;
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function()
{
    (function()
    {
        var parametros = getUrlVars();
        //Determino si llego el id de la ciudad
        if (parametros["idCiudad"])
        {
            var idCiudad, nombreCiudad, urlD, idCentroComercial, nombreCentroComercial,div;
            idCiudad = parametros["idCiudad"];
            nombreCiudad = parametros["nombreCiudad"];
            urlD = parametros["url"];
            idCentroComercial = parametros["idCentroComercial"];
            nombreCentroComercial = parametros["nombreCentroComercial"];
            div = parametros["div"];
            
            urlD = urlD + "?idCiudad=" + idCiudad + "&nombreCiudad=" + nombreCiudad + "&idCentroComercial=" + idCentroComercial + "&nombreCentroComercial=" + nombreCentroComercial+"&div="+div;
            getCategoriasByCentroComercial(urlD, idCentroComercial);
        }
    })();
    $(".menu").on("click","li",
            function(e)
            {
                e.preventDefault();
                var url=$(this).find("a").attr("href");
                redirigir(url);
            });
});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}


function getCategoriasByCentroComercial(urlD, idCentroComercial)
{

    mostrarDialogo();
    var estaVacio = true;
    var url = url_base + "centroscomerciales/getcetegoriasbycentrocomercial.xml";
    var datos = {
        idCentroComercial: idCentroComercial
    };
    ajax(url, datos,
            function(xml)
            {
                if (xml != null)
                {
                    var html = "<li class='todos'>" +
                            "<a href='$1&idCategoria=$2&nombreCategoria=$3' id='lnktodos'>$3</a>" +
                            "<a class='go'></a>" +
                            "</li>";
                    html = html.replace("$1", urlD);
                    html = html.replace("$2", 0);
                    html = html.replace("$3", "Todas");
                    html = html.replace("$3", "Todas");
                    $("#categorias").append(html);
                    $("datos", xml).each(function()
                    {

                        var obj = $(this).find("c");
                        var valor, texto, clases;
                        valor = $("id", obj).text();
                        texto = $("nombre", obj).text();
                        clases = $("class", obj).text();
                        console.log("Class:" + clases);
                        if (valor)
                        {
                            estaVacio = false;
                            html = "<li class='$5'>" +
                                    "<a href='$1&idCategoria=$2&nombreCategoria=$3' id='$4'>$3</a>" +
                                    "<a class='go'></a>" +
                                    "</li>";
                            html = html.replace("$1", urlD);
                            html = html.replace("$2", valor);
                            html = html.replace("$3", texto);
                            html = html.replace("$3", texto);
                            html = html.replace("$4", clases);
                            html = html.replace("$5", clases);
                            $("#categorias").append(html);
                        }

                    });
                }
                ocultarDialogo();
                if (estaVacio)
                    $("#categorias").html("Lo sentimos, no encontramos informacion");
            });


}