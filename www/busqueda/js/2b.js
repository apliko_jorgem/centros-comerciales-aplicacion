//Esta pagina se encarga de recibir el id de un local y se encarga de listar todos aquello centros comerciales en los que se encuentra dicho local
function onDeviceReady()
{
    //hide splash screen


}
;
document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function()
{
    $(function()
    {
        var parametros = getUrlVars();
        getBanner("2b", "null", "null", parametros["idLocal"], "../");
        getCentrosComercialesByLocal(parametros["idLocal"]);
    }
    );
    $(".menu").on("click", "li",
            function(e)
            {
                e.preventDefault();
                var url = $(this).find("a").attr("href");
                redirigir(url);
            });
});
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getCentrosComercialesByLocal(idLocal)
{
    mostrarDialogo();
    var estaVacio = true;
    var url = url_base + "almacenes/getcentroscomercialesbylocal.xml";
    var datos = {
        idLocal: idLocal
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("datos", xml).each(function()
            {
                var obj = $(this).find("cc");
                var nombreLocal, idLocal, nombreCC;
                nombreCC = $("nombre", obj).text();
                obj = $(this).find("a");
                idLocal = $("id", obj).text();
                nombreLocal = $("nombre", obj).text();
                if (idLocal)
                {
                    estaVacio = false;
                    var html = "<li class='todos'>" +
                            "<a href='3.html?idLocal=$1' id='lnktodos'>$2-$3</a>" +
                            "<a class='go'></a>" +
                            "</li>";
                    html = html.replace("$1", idLocal);
                    html = html.replace("$2", nombreLocal);
                    html = html.replace("$3", nombreCC);
                    $("#locales").append(html);
                }

            });
        }
        ocultarDialogo();
        if (estaVacio)
            $("#locales").append("Lo sentimos, no encontramos informacion");
    });


}