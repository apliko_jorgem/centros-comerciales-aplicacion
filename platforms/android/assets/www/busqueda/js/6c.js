/*Esta pagina se encarga de mostrar las promociones */
function onDeviceReady() {
    //hide splash screen

//    var parametros = getUrlVars();
    getNoticias();
}
//document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(
        function()
        {
            /*Funciones autoejecutables*/
            (function()
            {
                onDeviceReady();
            })();
        }
);
//function mostrarDialogo()
//{
//    $('#element_to_pop_up').bPopup();
//}
//function ocultarDialogo()
//{
//    $('#element_to_pop_up').bPopup().close();
//}
function getNoticias()
{

    mostrarDialogo();
    var estaVacio = true;
//    var url = url_base + "promociones/getpromociones.xml";
    var url = url_base + "noticias/index.xml";
    var datos = {
        
    };
    ajax(url, datos, function(xml)
    {
        if (xml != null)
        {
            $("#promociones").html("");
            $("datos", xml).each(function()
            {
                var obj = $(this).find("Noticia");

                var src, id;
                src = $("banner", obj).text();
                id = $("id", obj).text();
                if (id)
                {
                    estaVacio = false;
                    var html = "<a href='$2'><img src='$1' /></a><br><br><br>";
                    html = html.replace("$1", src);
                    html = html.replace("$2", "6d.html?idNoticia=" + id);
                    $("#promociones").append(html);
                }

            });
        }
        ocultarDialogo();
        if (estaVacio)
        {
            $("#promociones").html("Lo sentimos, no conseguimos informacion");
        }
    });


}